package com.acme.utils;

public class InvalidDateException extends Exception{

    public InvalidDateException() {

        super ("Exception: Attempted to set an invalid date.");

    }

}
