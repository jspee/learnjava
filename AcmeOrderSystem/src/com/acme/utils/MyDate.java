package com.acme.utils;

public class MyDate {
    private byte day;
    private byte month;
    private short year;
    private static MyDate[] holidays;

    static {
        // initialise public holiday set
        holidays = new MyDate[6];
        holidays[0] = new MyDate(1 , 1, 2021);
        holidays[1] = new MyDate(9 , 5, 2021);
        holidays[2] = new MyDate(5 , 30, 2021);
        holidays[3] = new MyDate(11 , 24, 2021);
        holidays[4] = new MyDate(7 , 4, 2021);
        holidays[5] = new MyDate(12 , 25, 2021);

    }

    public static MyDate[] getHolidays(){
        return holidays;
    }

    public void setYear(int year) {
        try {
            valid(1, 1, year);
            this.year = (short) year;
        }
        catch (InvalidDateException e){
            System.out.println("That year value creates an invalid date: "+ year);
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void setMonth(int month) {
        try {
            valid(1, month, 2020);
            this.month = (byte) month;
        }
        catch (InvalidDateException e){
            System.out.println("That month value creates an invalid date: "+ month);
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void setDay(int day) {
        try {
            valid(day, 1, 2020);
            this.day = (byte) day;
        }
        catch (InvalidDateException e){
            System.out.println("That day value creates an invalid date: "+ day);
            e.printStackTrace();
            System.exit(0);
        }
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    // CONSTRUCTORS
    public MyDate(){
        this(1, 1, 2000);
    }
    public MyDate(int m, int d, int y){
        try {
            this.valid(d, m, y);
            setDate(m, d, y);
        }
        catch (InvalidDateException e){
            System.out.println(e.getMessage());
            System.out.println("Invalid date detected: "+ d + "/" + m+ "/" +y);
            System.exit(0);
        }
    }

    private void valid(int day, int month, int year) throws InvalidDateException {
        boolean validDate = true;

        if (day > 31 || day < 1 || month > 12 || month < 1 || year < 0) {
            validDate = false;
            // System.out.println("Attempting to create a non-valid date " + month + "/" + day + "/" + year);
        }
        else {
            switch (month) {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (day > 30) validDate = false;
                case 2:
                    if (!( day <= 28 || (day == 29 && year % 4 == 0) )) validDate = false;
        }
    }
        if (!validDate) throw new InvalidDateException();
    }

    public static void listHolidays() {
        System.out.println("The holidays are:");
        for (MyDate element : holidays){
            System.out.println(element);
        }
    }

    public String toString(){
        //  return a string with month/day/year like “01/20/1964”
        return String.valueOf(month) + "/" +
                String.valueOf(day) + "/" +
                String.valueOf(year);
    }

    public void setDate(int m, int d, int y){
        // set the date value
        try{
            this.valid(d,m,y);
            this.setDay(d);
            this.setMonth(m);
            this.setYear(y);
        }
        catch (InvalidDateException e){
            System.out.println(e);
            System.out.println("These values create an invalid date: "+ d + "/" + m+ "/" +y);
            e.printStackTrace();
            System.exit(0);
        }

    }

    /**
     * List leap years
     */
    public static void leapYears() {
        for (int i = 1752; i < 2020; i = i+4) {
            if (    ((i % 100) != 0) ||
                     ((i % 400) == 0)
                )
                System.out.println("The year " + i + " is a leap year");
        }
    }
     public boolean equals (Object o){
        if (o instanceof MyDate){
            MyDate d = (MyDate) o;
            if ( (this.day == d.day) && (this.month == d.month) && (this.year == d.year) )
                return true;
        }
        return false;
    }


}
