package com.acme.testing;

import com.acme.utils.MyDate;

public class PassByExperiment {
    public static void main (String[] args){

        MyDate date = new MyDate(1,20,2008);

        // show that objects pass by reference, changes made in the
        // method are later visible from the calling method
        System.out.println("Before passing an object " + date);
        passObject(date);
        System.out.println("After passing an object " + date);

        // show that primatives passed to methods are passed by value,
        // changes made are not visible outside the scope of the called method
        System.out.println("Before passing a primitive " + date.getYear());
        passPrimitive(date.getYear());
        System.out.println("After passing a primitive " + date.getYear());

        // show that strings are passed by reference, BUT because they
        // are immutable the change is not visible from the calling method
        String x = date.toString();
        System.out.println("Before passing a String " + x);
        passString(x);
        System.out.println("After passing a String " + x);

        // create a pass String method that works with string builder
        // to run using less processor effort
        StringBuilder y = new StringBuilder(date.toString());
        System.out.println("Before passing a StringBuilder " + y);
        passStringBuilder(y);
        System.out.println("After passing a StringBuilder " + y);

    }

    public static void passObject(MyDate d){
        d.setYear(2009);
    }

    public static void passPrimitive(int i){
        i=2010;
    }

    public static void passString(String s){
        int yearSlash =s.lastIndexOf('/');
        s = s.substring(0, yearSlash+1);
        s += "2012";
        System.out.println("New date string: " + s);
    }

    public static void passStringBuilder(StringBuilder sb) {
        int yearSlash = sb.lastIndexOf("/");
        sb.replace(yearSlash + 1,sb.length(),"");
        sb.append("2012");
        System.out.println("New date stringBuilder: " + sb);
    }
}
