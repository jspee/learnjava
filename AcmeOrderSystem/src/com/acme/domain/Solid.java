package com.acme.domain;

public class Solid extends Good{
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    private double width;
    private double length;
    public Solid(String name, int modelNumber, double height, UnitOfMeasureType uoM, boolean flammable,
                 double wgtPerUoM, double length, double width) {
        super(name, modelNumber, height, uoM, flammable, wgtPerUoM);
        this.length = length;
        this.width = width;
    }

    public double volume(){
        return width * length * getHeight();
    }

    public String toString() {
        return super.toString() + " that is " + volume() + " " +
                getUnitOfMeasure() + " in size";
    }
}
