package com.acme.testing;

import com.acme.domain.Good;
import com.acme.domain.Liquid;
import com.acme.domain.Solid;
import com.acme.domain.Good.UnitOfMeasureType;

import java.util.*;

public class TestGoods {

	public static void main(String[] args) {
		Liquid glue = new Liquid("Acme Glue", 2334, 4, UnitOfMeasureType.LITER,
				false, 15, 6);
		Liquid paint = new Liquid("Acme Invisible Paint", 2490, 0.65, UnitOfMeasureType.GALLON, true, 0.70, 12);

		Solid anvil = new Solid("Acme Anvil", 1668, 0.3, UnitOfMeasureType.CUBIC_METER, false, 5000, 0.5,
				0.5);

		System.out.println(glue);
		System.out.println(paint);
		System.out.println(anvil);
		
		System.out.println("The weight of " + glue + " is " + glue.weight());
		System.out.println("The weight of " + paint + " is " + paint.weight());
		System.out.println("The weight of " + anvil + " is " + anvil.weight());

		Good x = glue;
		System.out.println("Is " + x + " flammable?  " + x.isFlammable());
		x = paint;
		System.out.println("Is " + x + " flammable?  " + x.isFlammable());

		// test the catalogue function
		System.out.println("original catalogue");
		System.out.println(Good.getCatalog());

		// remove from the catalogue
		System.out.println("remove invisible paint from catalogue");
		Iterator<Good> iterator = Good.getCatalog().iterator();
		while(iterator.hasNext())
		{
			if(iterator.next().getName().equals("Acme Invisible Paint"))
				iterator.remove();
		}
		System.out.println(Good.getCatalog());

		// add to the catalogue
		System.out.println("ADD TOASTER TO CATALOGUE");
		Solid toaster = new Solid("Acme Toaster", 1775, 0.75,
				UnitOfMeasureType.CUBIC_FEET, false, 1, 1, 1);
		Good.getCatalog().add(toaster);
		Good.getCatalog().add(toaster);  // ignored bc this is a hashset (no dupes)
		System.out.println(Good.getCatalog());

		// list flammable catalogue items
		System.out.println("LIST FLAMMABLES");
		System.out.println("Flammable products: " + Good.flammablesList());

		// list flammable catalogue items
		System.out.println("SORTED CATALOGUE");
		List<Good> list = new ArrayList<Good>(Good.getCatalog());
		Collections.sort(list);
		System.out.println(list);

		// search for a catalogue item that does not exist
		Good.searchByModelNumber(123);

		// search for a catalogue item that does exist
		Good.searchByModelNumber(4289);

	}
}
