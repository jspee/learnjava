package com.acme.testing;

import com.acme.utils.MyDate;

public class TestMyDate{

	public static void main(String[] args){
		MyDate date1 = new MyDate(11,11,1918);
		MyDate date2 = new MyDate();
		date2.setDay(11);
		date2.setMonth(11);
		date2.setYear(1918);

		// MyDate date3 = new MyDate();
		MyDate date3 = new MyDate(1,1,2021);
		// date3.setDate(4,21,1968);


		MyDate date4 = new MyDate();

		String str1 = date1.toString();
		String str2 = date2.toString();
		String str3 = date3.toString();
		String str4 = date4.toString();

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);


		// MyDate.leapYears();

		MyDate newYear = new MyDate(1,1,2009);
		MyDate fiscalStart = new MyDate(1,1,2009);

		if (newYear.equals(fiscalStart))
			System.out.println("These two dates are equal");
		else
			System.out.println("These two dates are not equal");

		// date3.setMonth(13); // bad month
		// date3.setDay(32); // bad day
		// date3.setYear(-1); // bad year
		// date3.setDate(13, 1, 2021); // bad date
		// date3.setDate(2, 29, 2020); // valid date - leap year
		// date3.setDate(2, 29, 2021); // bad date
		date3.setDate(13, 40, -1);


	}

}