package com.acme.domain;

import com.acme.utils.HolidayOrdersNotAllowedException;
import com.acme.utils.MyDate;

public class Order {

    private static double taxRate;
    private String customer;
    private Product product;
    private int quantity;
    private double orderAmount = 0.00;
    private MyDate orderDate;
    private static Rushable rushable;

    static {
        // use static initialisation to run a static method
        setTaxRate(0.05);
    }

    // getters and setters
    public static Rushable getRushable()
    {
        return rushable;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public MyDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(MyDate orderDate) throws HolidayOrdersNotAllowedException {
        if (isHoliday(orderDate)) this.orderDate = orderDate;
        else {
            System.out.println("Order date, " + orderDate + ", cannot be set to a holiday!");
            throw new HolidayOrdersNotAllowedException(orderDate);
        }
    }

    public static double getTaxRate() {
        return taxRate;
    }

    public static void setRushable(Rushable rushable) {Order.rushable = rushable;}

    public double getOrderAmount() {return orderAmount;}

    public void setQuantity(int quantity) {
        if (quantity >= 0)
            this.quantity = quantity;
        else
            System.out.println("Attempting to create an order quantity less than zero");
    }

    public boolean isPriorityOrder()
    {
        boolean priorityOrder = false;
        if( rushable != null ) {
            priorityOrder = rushable.isRushable(orderDate, orderAmount);
        }
        return priorityOrder;
    }

    public void setOrderAmount(double orderAmount) {
        if (orderAmount >= 0)
            this.orderAmount = orderAmount;
        else
            System.out.println("Attempting to create an order amount less than zero");
    }

    /**
     * Create an Order
     *
     * @param d   date of order
     * @param amt value of order
     * @param c   customer
     * @param p   product
     * @param q   quantity of product
     */
    public Order(MyDate d, double amt, String c, Product p, int q) {
        orderAmount = amt;
        customer = c;
        product = p;
        quantity = q;
        try {setOrderDate(d);}
        catch (HolidayOrdersNotAllowedException e){
            System.out.println("You cannot create an order on this date. Application closing");
            System.exit(0);
        }
    }

    public String toString() {
        return quantity + " ea. " + product + " for " + customer;
    }

    static public void setTaxRate(double newTaxRate) {
        taxRate = newTaxRate;
    }

    static public void computeTaxOn(double anAmount) {
        System.out.println("The tax for " + anAmount + " is: " + anAmount * Order.taxRate);
    }

    public double computeTax() {
        double taxOnOrder = this.orderAmount * Order.taxRate;
        System.out.println("The tax for this order is: " + taxOnOrder);
        return taxOnOrder;
    }

    /**
     * Calculate job size (S, M, L, X)
     *
     * @return A single char describing the size of the order
     */
    public char jobSize() {
        char size;

        if (this.quantity <= 25)
            size = 'S';
        else if (this.quantity <= 75)
            size = 'M';
        else if (this.quantity <= 150)
            size = 'L';
        else
            size = 'X';

        return size;
    }

    /**
     * Calculate the charge for the order
     *
     * @return The amount to be billed, after discounts
     */
    public double computeTotal() {
        double jobSizeDiscountPercent;
        switch (this.jobSize()) {
            case 'M':
                jobSizeDiscountPercent = 0.01;
                break;
            case 'L':
                jobSizeDiscountPercent = 0.02;
                break;
            case 'X':
                jobSizeDiscountPercent = 0.03;
                break;
            default:
                jobSizeDiscountPercent = 0;
                break;
        }
        double preTaxValue = (this.orderAmount * (1 - jobSizeDiscountPercent));
        double finalValue = (preTaxValue < 1500) ? preTaxValue + this.computeTax() : preTaxValue;
        return finalValue;
    }

    /**
     * Check if the order date is on a public holiday
     *
     * @return True if the order date is on a public holiday
     */
    private boolean isHoliday(MyDate proposedDate){
        boolean dateIsHoliday = false;
        for (MyDate holiday : proposedDate.getHolidays()){
            if (proposedDate.equals(holiday)) dateIsHoliday = true;
        }
        return dateIsHoliday;
    }

}
